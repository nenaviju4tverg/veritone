This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).
`[
## Tech stack:
1. React + NextJS
2. Redux toolkit
3. MaterialUI
4. NodeJS + Express
5. MongoDB

## Getting Started:
### Installation
```bash
npm i
```
### Run services
#### MongoDB:
```bash
sudo systemctl start mongod
```
#### Front-end:
 ```bash
 npm run dev
 ```
Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

#### Back-end:
 ```bash
 npm run server
 ```
API available with [http://localhost:4000](http://localhost:4000)
