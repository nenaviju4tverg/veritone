const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser')
const morgan = require('morgan');

const app = express();
require("./mongoose.js")(app);
app.use(morgan('dev'));
app.use(cors());
app.use(bodyParser.json())

const shoppingListRoutes = require("./router")
app.use("/api/list", shoppingListRoutes);

const port = process.env.PORT || 4000;

app.listen(port, () => {
  console.log(`Your app is listening on port ${port}`);
});
