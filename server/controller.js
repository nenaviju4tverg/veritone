const ShoppingList = require("./model");
exports.getList = async () => {
  const list = await ShoppingList.find();
  return list;
};
exports.itemById = async id => {
  const listItem = await ShoppingList.findById(id);
  return listItem;
}
exports.createItem = async payload => {
  const newListItem = await ShoppingList.create(payload);
  return newListItem
}
exports.updateItem = async (id, payload) => {
  const newListItem = await ShoppingList.findByIdAndUpdate(id, payload);
  return newListItem
}
exports.removeItem = async id => {
  const listItem = await ShoppingList.findByIdAndRemove(id);
  return listItem
}
