const router = require("express").Router();
const shoppingList = require('./controller')

router.post("/", async (req, res) => {
  try {
    let payload = {
      title: req.body.title,
      description: req.body.description,
      count: req.body.count,
      isPurchased: req.body.isPurchased,
    }
    let listItem = await shoppingList.createItem({
      ...payload
    });
    res.status(200).json({
      status: true,
      data: listItem,
    })
  } catch (err) {
    console.log(err)
    res.status(500).json({
      error: err,
      status: false,
    })
  }
});

router.put("/:id", async (req, res) => {
  try {
    let id = req.params.id
    let payload = {
      title: req.body.title,
      description: req.body.description,
      count: req.body.count,
      isPurchased: req.body.isPurchased,
    }
    let listItem = await shoppingList.updateItem(id, {
      ...payload
    });
    res.status(200).json({
      status: true,
      data: listItem,
    })
  } catch (err) {
    console.log(err)
    res.status(500).json({
      error: err,
      status: false,
    })
  }
});

router.get("/", async (req, res) => {
  try {
    let list = await shoppingList.getList();
    res.status(200).json({
      status: true,
      data: list,
    })
  } catch (err) {
    console.log(err)
    res.status(500).json({
      error: err,
      status: false,
    })
  }
});

router.get("/:id", async (req, res) => {
  try {
    let id = req.params.id
    let item = await shoppingList.itemById(id);
    res.status(200).json({
      status: true,
      data: item,
    })
  } catch (err) {
    res.status(500).json({
      status: false,
      error: err
    })
  }
});

router.delete("/:id", async (req, res) => {
  try {
    let id = req.params.id
    let item = await shoppingList.removeItem(id)
    res.status(200).json({
      status: true,
      data: item,
    })
  } catch (err) {
    res.status(500).json({
      status: false,
      error: err
    })
  }
});

module.exports = router;
