const mongoose = require("mongoose");
const shoppingListSchema = mongoose.Schema({
  title: {
    type: String,
    required: [true, "Please include the shopping item title"],
  },
  description: {
    type: String,
    required: false,
    default: ''
  },
  count: {
    type: Number,
    required: [true, "Please include the needed number of items"],
    min: 1,
    max: 3
  },
  isPurchased: {
    type: Boolean,
    required: false,
    default: false
  },
});

const ShoppingList = mongoose.model("Shopping List", shoppingListSchema);
module.exports = ShoppingList;
