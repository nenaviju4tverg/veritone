import React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';

import {setCreateDialogOpen} from "../../reducers/createDialog";
import {useAppDispatch} from '../../hooks'

const EmptyList = () => {
  const dispatch = useAppDispatch();

  return (
    <Box sx={{
      maxWidth: '614px',
      width: '100%',
      border: '1px solid #C6C6C6',
      borderRadius: '5px',
      marginTop: '110px',
      marginX: 'auto',
      paddingTop: '87px',
      paddingBottom: '127px',
    }}>
      <Box sx={{}}>
        <Typography
          variant="h6"
          noWrap
          component="div"
          textAlign="center"
          sx={{ fontFamily: "Nunito", fontWeight: 400, fontSize: '18px', color: '#87898C', marginBottom: '16px' }}
        >
          Your shopping list is empty :(
        </Typography>
      </Box>
      <Box sx={{
        display: 'flex',
        justifyContent: 'center',
      }}>
        <Button variant="contained" sx={{fontFamily: "Nunito", textTransform: 'none'}} onClick={() => dispatch(setCreateDialogOpen(true))}>Add your first item</Button>
      </Box>
    </Box>
  );
}

export default EmptyList
