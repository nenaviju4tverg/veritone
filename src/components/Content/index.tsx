import React, {useEffect, useState} from "react";

import Loading from "../ContentLoading/index";
import EmptyList from "../EmptyList/inex";
import ShoppingList from "../ShoppingList";
import {useAppSelector} from "../../hooks";
import { selectDeleteDialogOpen } from "../../reducers/deleteDialog";
import { selectCreateDialogOpen } from "../../reducers/createDialog";

export interface ShoppingListItemInterface {
  title: string,
  description: string,
  count: number,
  id?: string,
  isPurchased: boolean,
}

const Content = () => {
  const [shoppingList, setShoppingList] = useState<ShoppingListItemInterface[]>([]);
  const [shoppingListError, setShoppingListError] = useState("");
  const [shoppingListLoading, setShoppingListLoading] = useState(false);

  const deleteDialogOpen = useAppSelector(selectDeleteDialogOpen);
  const createDialogOpen = useAppSelector(selectCreateDialogOpen);

  const handleShoppingList = async () => {
    setShoppingListLoading(true);
    try {
      const response = await fetch(
        "http://127.0.0.1:4000/api/list"
      );
      const result = await response.json()
      setShoppingList(result.data);
    } catch (err: any) {
      setShoppingListError(err.message || "Unexpected Error!");
    } finally {
      setShoppingListLoading(false);
    }
  };

  useEffect(() => {
    handleShoppingList();
  }, [deleteDialogOpen, createDialogOpen]);

  if (shoppingListLoading) {
    return <Loading/>
  }

  if (shoppingListError !== '') {
    return <div>Error: {shoppingListError}</div>
  }

  return (
    <>
      {shoppingList.length > 0 ? <ShoppingList shoppingList={shoppingList} handleShoppingList={handleShoppingList}/> : <EmptyList/>}
    </>
  )
}

export default Content
