import React from 'react'
import ListHeader from './ShoppingListHeader'
import ShoppingListItem from './ShoppingListItem'
import Box from "@mui/material/Box";
import List from '@mui/material/List';

import { ShoppingListItemInterface } from '../Content'

interface Props {
  shoppingList: ShoppingListItemInterface[]
  handleShoppingList: () => void
}

const ShoppingList: React.FC<Props> = ({shoppingList, handleShoppingList}) => {
  return (
    <Box
    sx={{width: '1024px', marginX: 'auto', paddingTop: '35px', paddingBottom: '11px'}}
    >
      <ListHeader />
      <List>
        {
          shoppingList.map(({ title, description, count, isPurchased, id}) => id && (<ShoppingListItem
              title={title} description={description} id={id} isPurchased={isPurchased} key={id} handleShoppingList={handleShoppingList} count={count}
            />))
        }
      </List>
    </Box>
  )
}

export default ShoppingList;
