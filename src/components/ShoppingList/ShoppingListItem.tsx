import React from 'react'
import Box from "@mui/material/Box";
import ListItem from '@mui/material/ListItem';
import Checkbox from "@mui/material/Checkbox";
import ListItemText from "@mui/material/ListItemText";
import IconButton from '@mui/material/IconButton';
import Edit from '@mui/icons-material/Edit';
import Delete from '@mui/icons-material/Delete';

import {useAppDispatch} from "../../hooks";
import { setDeleteDialogOpen, setDeleteDialogId} from "../../reducers/deleteDialog";
import { setCreateDialogOpen, setCreateDialogId} from "../../reducers/createDialog";

interface ShoppingListItemProp {
  title: string,
  description: string,
  isPurchased: boolean,
  id: string
  count: number,
  handleShoppingList: () => void
}

const ShoppingListItem: React.FC<ShoppingListItemProp> = ({title,
                                                            description ,
                                                            isPurchased,
                                                            id, count, handleShoppingList}) => {
  const dispatch = useAppDispatch();

  const setPurchased = async () => {
    try {
      const requestOptions = {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          title,
          description,
          isPurchased: !isPurchased,
          count
        })
      };
      const url = `http://127.0.0.1:4000/api/list/${id}`
      await fetch(url, requestOptions)
    } catch (err: any) {
      console.error(err)
    }
    handleShoppingList()
  }

  const handleDelete = () => {
    dispatch(setDeleteDialogId(id))
    dispatch(setDeleteDialogOpen(true))
  }

  const handleEdit = () => {
    dispatch(setCreateDialogId(id))
    dispatch(setCreateDialogOpen(true))
  }

  return (
    <Box>
      <ListItem
        sx={{
          paddingY: '12px',
          border: isPurchased ? '0' : '0.5px solid #D5DFE9',
          borderRadius: '4px',
          marginBottom: '12px'
        }}
        secondaryAction={
        <>
          <IconButton aria-label="edit" onClick={handleEdit}>
            <Edit/>
          </IconButton>
          <IconButton edge="end" aria-label="delete" onClick={handleDelete}>
            <Delete/>
          </IconButton>
        </>
        }
        selected={isPurchased}
      >
        <Box sx={{
          marginRight: '12px'
        }}>
          <Checkbox
            checked={isPurchased}
            onChange={setPurchased}
          />
        </Box>
        <ListItemText primary={title} secondary={description} />
      </ListItem>
    </Box>
  )
}

export default ShoppingListItem;
