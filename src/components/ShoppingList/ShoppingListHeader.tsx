import React from 'react'
import Box from '@mui/material/Box';
import Toolbar from "@mui/material/Toolbar";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";

import {useAppDispatch} from "../../hooks";
import { setCreateDialogOpen, setCreateDialogId} from "../../reducers/createDialog";

const ShoppingListHeader = () => {
  const dispatch = useAppDispatch();

  const handleCreate = () => {
    dispatch(setCreateDialogOpen(true))
  }

  return (
    <Box>
      <Toolbar sx={{
        justifyContent: "space-between",
      }}>
        <Typography fontFamily='Nunito' fontSize='18px' fontWeight={600}>
          Your Items
        </Typography>
        <Button variant="contained" sx={{
          fontFamily: "Nunito",
          textTransform: 'none'
        }} onClick={handleCreate}>Add Item</Button>
      </Toolbar>
    </Box>
  )
}

export default ShoppingListHeader;
