import React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';


const Header = () => {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar
          sx={{ paddingX: '30px', backgroundColor: '#4D81B7'}}
        >
          <Typography
            variant="h6"
            noWrap
            component="div"
            sx={{ fontFamily: "Dosis", fontWeight: 600 }}
          >
            SHOPPING LIST
          </Typography>
        </Toolbar>
      </AppBar>
    </Box>
  );
}

export default Header;
