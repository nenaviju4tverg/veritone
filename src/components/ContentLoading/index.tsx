import React from 'react';
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';

const Loading = () => {
  return (
    <Box sx={{ display: 'flex', justifyContent: "center", paddingTop: '124px'}}>
      <CircularProgress size={76} thickness={3} />
    </Box>
  );
}

export default Loading
