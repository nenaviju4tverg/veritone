import React, {useEffect} from 'react'
import Button from "@mui/material/Button";
import LoadingButton from "@mui/lab/LoadingButton";
import Typography from "@mui/material/Typography";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import TextField from "@mui/material/TextField";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import InputLabel from "@mui/material/InputLabel";
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import * as Yup from 'yup';
import {
  FormikHelpers,
  useFormik
} from 'formik';

import {useAppSelector, useAppDispatch} from '../../hooks'
import {setCreateDialogOpen, setCreateDialogId, selectCreateDialogOpen, selectCreateDialogId} from "../../reducers/createDialog";

interface FormValues {
  title: string,
  description: string,
  count: number,
  isPurchased: boolean,
}

const CreateListItem = () => {
  const [createDialogLoading, setCreateDialogLoading] = React.useState(false)
  // TODO: possible usage, skip for now
  const [createDialogError, setCreateDialogError] = React.useState('')


  const dispatch = useAppDispatch();
  const open = useAppSelector(selectCreateDialogOpen);
  const itemId = useAppSelector(selectCreateDialogId);

  const initialValues: FormValues = {
    title: '',
    description: '',
    count: 0,
    isPurchased: false
  };

  const validationSchema = Yup.object().shape({
    title: Yup.string()
      .required('Required'),
    description: Yup.string()
      .min(0)
      .max(100),
    count: Yup.number()
      .min(1)
      .max(3)
      .required('Required'),
    isPurchased: Yup.boolean().required('Required'),
  });

  const formik = useFormik({
    initialValues: initialValues,
    validationSchema: validationSchema,
    onSubmit: (
      values: FormValues,
      { setSubmitting }: FormikHelpers<FormValues>
    ) => {
      setSubmitting(false);
      handleSaveAndClose()
    },
  });

  const createListItem = async () => {
    setCreateDialogLoading(true);
    try {
      // POST request using fetch inside useEffect React hook
      const requestOptions = {
        method: itemId ? 'PUT' : 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(formik.values)
      };
      const url = itemId ? `http://127.0.0.1:4000/api/list/${itemId}` : `http://127.0.0.1:4000/api/list`
      await fetch(url, requestOptions)
    } catch (err: any) {
      setCreateDialogError(err.message || "Unexpected Error!");
    } finally {
      setCreateDialogLoading(false);
    }
  };

  const handleClose = () => {
    formik.resetForm();
    dispatch(setCreateDialogOpen(false));
    dispatch(setCreateDialogId(undefined));
  };

  const handleSaveAndClose = async () => {
    await createListItem()
    handleClose()
  }

  const fetchListItem = async () => {
    setCreateDialogLoading(true);
    try {
      const response = await fetch(
        `http://127.0.0.1:4000/api/list/${itemId}`
      );
      const result = await response.json()
      const {title, description, count, isPurchased}: FormValues  = result.data
      await formik.setValues({title, description, count, isPurchased})
    } catch (err: any) {
      setCreateDialogError(err.message || "Unexpected Error!");
    } finally {
      setCreateDialogLoading(false);
    }
  };

  useEffect(() => {
    if (itemId) {
      fetchListItem();
    }
  }, [itemId])

  return (
    <Dialog
      open={open}
      onClose={handleClose}
    >
      <DialogTitle sx={{
        backgroundColor: '#FAFAFA',
        borderBottom: '1px solid #D5DFE9'
      }}>SHOPPING LIST</DialogTitle>
      <DialogContent sx={{
        paddingTop: '28px !important'
      }}>
        <form onSubmit={formik.handleSubmit}>
          <Typography sx={{fontSize: '18px'}}>Add an Item</Typography>
          <Typography sx={{fontSize: '16px', marginBottom: '8px'}}>Add your new item below</Typography>
          <TextField sx={{
            marginBottom: '18px'
          }} fullWidth id="itemName" label="Item Name" variant="outlined" name="title"
           value={formik.values.title}
           onChange={formik.handleChange}
           error={formik.touched.title && Boolean(formik.errors.title)}
           helperText={formik.touched.title && formik.errors.title}
          />
          <TextField sx={{
            marginBottom: '18px'
          }} fullWidth id="description" rows={4} label="Description" multiline variant="outlined" name="description"
                     value={formik.values.description}
                     onChange={formik.handleChange}
                     error={formik.touched.description && Boolean(formik.errors.description)}
                     helperText={formik.touched.description && formik.errors.description}
          />
          <FormControl fullWidth sx={{
            marginBottom: '30px'
          }}>
            <InputLabel id="labelId">How many</InputLabel>
            <Select
              labelId="labelId"
              fullWidth
              id="howMany"
              label="How many"
              value={formik.values.count}
              onChange={formik.handleChange}
              name="count"
              error={formik.touched.count && Boolean(formik.errors.count)}
            >
              <MenuItem value="0" disabled>How many</MenuItem>
              <MenuItem value="1">1</MenuItem>
              <MenuItem value="2">2</MenuItem>
              <MenuItem value="3">3</MenuItem>
            </Select>
          </FormControl>
          <FormGroup>
            <FormControlLabel control={<Checkbox value={formik.values.isPurchased}
                                                 onChange={formik.handleChange} name="isPurchased"
            />} label="Purchased" />
          </FormGroup>
        </form>
      </DialogContent>
      <DialogActions>
        <Button sx={{
          fontFamily: "Nunito",
          textTransform: 'none'
        }} onClick={handleClose}>Cancel</Button>
        <LoadingButton variant="contained"
          loading={createDialogLoading} type="submit" sx={{
          fontFamily: "Nunito",
          textTransform: 'none'
        }} onClick={() => formik.handleSubmit()}>Save Item</LoadingButton>
      </DialogActions>
    </Dialog>
  )
}

export default CreateListItem;
