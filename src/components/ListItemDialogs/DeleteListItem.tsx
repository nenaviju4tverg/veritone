import React from 'react'
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import LoadingButton from "@mui/lab/LoadingButton";

import {useAppDispatch, useAppSelector} from "../../hooks";
import {selectDeleteDialogId, selectDeleteDialogOpen, setDeleteDialogOpen} from "../../reducers/deleteDialog";


const DeleteListItem = () => {
  const [deleteDialogLoading, setDeleteDialogLoading] = React.useState(false)
  // TODO: possible usage, skip for now
  const [deleteDialogError, setDeleteDialogError] = React.useState('')

  const dispatch = useAppDispatch();
  const open = useAppSelector(selectDeleteDialogOpen);
  const itemId = useAppSelector(selectDeleteDialogId);

  const deleteListItem = async () => {
    if (!itemId) return
    setDeleteDialogLoading(true);
    try {
      const requestOptions = {
        method: 'DELETE',
        headers: { 'Content-Type': 'application/json' },
      };
      const url = `http://127.0.0.1:4000/api/list/${itemId}`
      await fetch(url, requestOptions)
    } catch (err: any) {
      setDeleteDialogError(err.message || "Unexpected Error!");
    } finally {
      setDeleteDialogLoading(false);
    }
  };

  const handleClose = () => {
    dispatch(setDeleteDialogOpen(false));
  };

  const handleDeleteAndClose = async () => {
    await deleteListItem()
    handleClose()
  };

  return (
    <Dialog
      open={open}
      onClose={handleClose}
    >
      <DialogTitle>Delete Item?</DialogTitle>
      <DialogContent sx={{
        paddingTop: '28px !important'
      }}>
        <Typography sx={{fontSize: '18px'}}>Add an Item</Typography>
        <DialogContentText>Are you sure you want to delete this item? This can not be undone.</DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button sx={{
          fontFamily: "Nunito",
          textTransform: 'none'
        }} onClick={handleClose}>Cancel</Button>
        <LoadingButton variant="contained" sx={{
          fontFamily: "Nunito",
          textTransform: 'none'
        }} loading={deleteDialogLoading} onClick={handleDeleteAndClose}>Delete</LoadingButton>
      </DialogActions>
    </Dialog>
  )
}

export default DeleteListItem;
