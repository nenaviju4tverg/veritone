import {createSlice, PayloadAction} from "@reduxjs/toolkit";

import type {RootState} from '../store'

export interface CreateDialogInterface {
  open: boolean,
  itemId?: string,
}

const initialState: CreateDialogInterface = {
  open: false,
}

export const createDialogSlice = createSlice({
  name: 'createDialog',
  initialState,
  reducers: {
    setCreateDialogId: (state, action: PayloadAction<string | undefined>) => {
      state.itemId = action.payload
    },
    setCreateDialogOpen: (state, action: PayloadAction<boolean>) => {
      state.open = action.payload
    }
  }
})

export const { setCreateDialogId, setCreateDialogOpen } = createDialogSlice.actions;

export const selectCreateDialogId = (state: RootState) => state.createDialog.itemId;
export const selectCreateDialogOpen = (state: RootState) => state.createDialog.open;

export default createDialogSlice.reducer
