import {createSlice, PayloadAction} from "@reduxjs/toolkit";

import type {RootState} from '../store'

export interface DeleteDialogInterface {
  open: boolean,
  itemId?: string,
}

const initialState: DeleteDialogInterface = {
  open: false,
}

export const deleteDialogSlice = createSlice({
  name: 'deleteDialog',
  initialState,
  reducers: {
    setDeleteDialogOpen: (state, action: PayloadAction<boolean>) => {
      state.open = action.payload
    },
    setDeleteDialogId: (state, action: PayloadAction<string>) => {
      state.itemId = action.payload
    }
  }
})

export const { setDeleteDialogOpen, setDeleteDialogId } = deleteDialogSlice.actions;

export const selectDeleteDialogOpen = (state: RootState) => state.deleteDialog.open;
export const selectDeleteDialogId = (state: RootState) => state.deleteDialog.itemId;

export default deleteDialogSlice.reducer
