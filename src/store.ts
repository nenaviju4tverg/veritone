import {configureStore} from '@reduxjs/toolkit';
import throttle from 'lodash/throttle'

import createDialogSlice from './reducers/createDialog'
import deletDialogSlice from './reducers/deleteDialog'

export const store = configureStore({
  reducer: {
    createDialog: createDialogSlice,
    deleteDialog: deletDialogSlice,
  }
})

export type RootState =  ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

store.subscribe(
  throttle(() => {}, 1000)
)
